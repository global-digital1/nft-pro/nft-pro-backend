<?php

use App\Http\Controllers\AssetController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\CollectionAssetController;
use App\Http\Controllers\CollectionController;
use App\Http\Controllers\ConfigController;
use App\Http\Controllers\CreatedAssetController;
use App\Http\Controllers\LikedAssetController;
use App\Http\Controllers\MintController;
use App\Http\Controllers\NftOfTheDayController;
use App\Http\Controllers\OwnedAssetController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\RevenueCatWebHookController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::post('auth/login', [AuthController::class, 'loginOld']);
Route::get('auth/message', [AuthController::class, 'message']);
Route::post('auth/verify', [AuthController::class, 'login']);
Route::post('revenue-cat', [RevenueCatWebHookController::class, 'handle']);
Route::get('config', [ConfigController::class, 'show']);
Route::middleware('auth:sanctum')->group(function () {
    Route::get('nft-of-the-day', [NftOfTheDayController::class, 'index']);
    Route::get('profile', [ProfileController::class, 'show']);
    Route::post('profile', [ProfileController::class, 'update']);
    Route::post('mint', [MintController::class, 'create']);
    Route::get('assets', [AssetController::class, 'index']);
    Route::get('created-assets', [CreatedAssetController::class, 'index']);
    Route::get('owned-assets', [OwnedAssetController::class, 'index']);
    Route::get('assets/{id}', [AssetController::class, 'show']);
    Route::get('collections', [CollectionController::class, 'index']);
    Route::get('collections/{id}', [CollectionController::class, 'show']);
    Route::get('collections/{id}/assets', [CollectionAssetController::class, 'index']);
    Route::get('liked-assets', [LikedAssetController::class, 'index']);
    Route::post('liked-assets/{id}', [LikedAssetController::class, 'create']);
    Route::delete('liked-assets/{id}', [LikedAssetController::class, 'destroy']);
});
