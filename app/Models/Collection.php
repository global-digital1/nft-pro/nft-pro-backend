<?php

namespace App\Models;

use App\Models\Builders\CollectionBuilder;
use Illuminate\Database\Eloquent\Model;

class Collection extends Model
{
    protected $perPage = 20;

    protected $fillable = [
        'slug',
        'image_url',
        'banner_image_url',
        'featured_image_url',
        'large_image_url',
        'discord_url',
        'telegram_url',
        'wiki_url',
        'twitter_username',
        'instagram_username',
        'name',
        'external_link',
        'description',
        'one_day_volume',
        'one_day_change',
        'one_day_sales',
        'one_day_average_price',
        'seven_day_volume',
        'seven_day_change',
        'seven_day_sales',
        'seven_day_average_price',
        'thirty_day_volume',
        'thirty_day_change',
        'thirty_day_sales',
        'thirty_day_average_price',
        'total_volume',
        'total_sales',
        'count',
        'num_owners',
        'average_price',
        'num_reports',
        'market_cap',
        'total_supply',
        'floor_price',
        'created_date',
    ];

    protected $casts = [
        'floor_price' => 'float',
        'one_day_volume' => 'float',
        'one_day_average_price' => 'float',
        'one_day_change' => 'float',
        'seven_day_volume' => 'float',
        'seven_day_average_price' => 'float',
        'seven_day_change' => 'float',
        'thirty_day_volume' => 'float',
        'thirty_day_average_price' => 'float',
        'thirty_day_change' => 'float',
        'total_volume' => 'float',
        'average_price' => 'float',
        'market_cap' => 'float',
    ];

    protected $dates = [
        'created_date',
    ];

    public function newEloquentBuilder($query)
    {
        return new CollectionBuilder($query);
    }

    public function assets()
    {
        return $this->hasMany(Asset::class, 'collection_slug', 'slug');
    }
}
