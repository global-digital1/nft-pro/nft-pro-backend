<?php

namespace App\Models;

use App\Models\Builders\AssetBuilder;
use Illuminate\Database\Eloquent\Model;

class Asset extends Model
{
    protected $perPage = 20;

    protected $fillable = [
        'token_id',
        'num_sales',
        'owner_address',
        'contract_address',
        'collection_slug',
        'image_url',
        'image_preview_url',
        'image_thumbnail_url',
        'image_original_url',
        'name',
        'external_link',
        'description',
        'created_date',
        'schema_name'
    ];

    protected $casts = [
        'is_liked' => 'bool'
    ];

    protected $dates = [
        'created_date'
    ];

    public function newEloquentBuilder($query)
    {
        return new AssetBuilder($query);
    }

    public function likedBy()
    {
        return $this->belongsToMany(User::class, 'likes')
            ->withPivot(['created_at'])
            ->withTimestamps();
    }

    public function collection()
    {
        return $this->belongsTo(Collection::class, 'collection_slug', 'slug');
    }

    public function owner()
    {
        return $this->belongsTo(User::class, 'owner_address', 'wallet_address');
    }
}
