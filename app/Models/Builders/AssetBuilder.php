<?php

namespace App\Models\Builders;

use Illuminate\Database\Eloquent\Builder;

class AssetBuilder extends Builder
{
    public function withIsLiked($userId)
    {
        return $this->withCount([
            'likedBy as is_liked' => function ($query) use ($userId) {
                $query->whereKey($userId);
            },
        ]);
    }

    public function filter($filter)
    {
        if ($filter) {
            $this->where(function ($query) use ($filter) {
                $query->where('name', 'like', '%' . $filter . '%')
                      ->orWhere('contract_address', 'like', '%' . $filter . '%')
                      ->orWhere('owner_address', 'like', '%' . $filter . '%');
            });
        }
        return $this;
    }

    public function order($order, $direction)
    {
        if ($order) {
            $this->orderBy($order, $direction ?: 'asc');
        }
        return $this;
    }
}
