<?php

namespace App\Models\Builders;

use Illuminate\Database\Eloquent\Builder;

class CollectionBuilder extends Builder
{
    public function filter($filter)
    {
        if ($filter) {
            $this->where(function ($query) use ($filter) {
                $query->where('name', 'like', '%' . $filter . '%')
                      ->orWhere('slug', 'like', '%' . $filter . '%');
            });
        }
        return $this;
    }

    public function order($order, $direction)
    {
        if ($order) {
            $this->orderBy($order, $direction ?: 'asc');
        }
        return $this;
    }
}
