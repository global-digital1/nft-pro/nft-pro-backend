<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EthPrice extends Model
{
    protected $fillable = [
        'ethbtc',
        'ethbtc_updated_at',
        'ethusd',
        'ethusd_updated_at',
    ];

    protected $dates = [
        'ethbtc_updated_at',
        'ethusd_updated_at',
    ];

    public $timestamps = false;
}
