<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NftOfTheDay extends Model
{
    protected $table = 'nft_of_the_day';

    protected $dates = ['date'];

    public $timestamps = false;

    public function asset()
    {
        return $this->belongsTo(Asset::class);
    }
}
