<?php

namespace App\Console\Commands;

use App\Models\EthPrice;
use App\Services\EtherScan;
use Illuminate\Console\Command;
use Illuminate\Support\Carbon;

class FetchLastEthPrice extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'eth-prices:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(EtherScan $scan)
    {
        $price = $scan->getLastPrice();
        EthPrice::query()->create([
            'ethbtc' => $price['ethbtc'],
            'ethbtc_updated_at' => Carbon::createFromTimestamp($price['ethbtc_timestamp']),
            'ethusd' => $price['ethusd'],
            'ethusd_updated_at' => Carbon::createFromTimestamp($price['ethusd_timestamp']),
        ]);
    }
}
