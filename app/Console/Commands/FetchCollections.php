<?php

namespace App\Console\Commands;

use App\Services\OpenSea\OpenSea;
use App\Services\OpenSea\OpenSeaSyncronizer;
use Illuminate\Console\Command;
use Illuminate\Support\Arr;

class FetchCollections extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'open-sea:collections:fetch';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(OpenSea $openSea)
    {
        $offset = 0;
        $limit  = 300;
        do {
            $collectionResponse = $openSea->getCollections(['offset' => $offset, 'limit' => $limit]);
            $offset   += $limit;
            foreach ($collectionResponse['collections'] as $collection) {
                OpenSeaSyncronizer::syncCollection($collection);
                $cursor = null;
                do {
                    $assetResponse = $openSea->getAssets([
                        'cursor'     => $cursor,
                        'limit'      => 200,
                        'collection' => $collection['slug'],
                    ]);
                    foreach ($assetResponse['assets'] as $asset) {
                        OpenSeaSyncronizer::syncAsset($asset);
                    }
                    usleep(250);
                } while ($cursor = Arr::get($assetResponse, 'next'));
            }
            usleep(250);
        } while (count(Arr::get($collectionResponse, 'collections')) > 0);
    }
}
