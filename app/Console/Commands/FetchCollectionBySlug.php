<?php

namespace App\Console\Commands;

use App\Services\OpenSea\OpenSea;
use App\Services\OpenSea\OpenSeaSyncronizer;
use Illuminate\Console\Command;
use Illuminate\Support\Arr;

class FetchCollectionBySlug extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'open-sea:collection:fetch {slug}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(OpenSea $openSea)
    {
        $collection = $openSea->getCollection($this->argument('slug'))['collection'];
        OpenSeaSyncronizer::syncCollection($collection);
        $cursor = null;
        do {
            $assetResponse = $openSea->getAssets([
                'cursor'     => $cursor,
                'limit'      => 200,
                'collection' => $collection['slug'],
            ]);
            foreach ($assetResponse['assets'] as $asset) {
                OpenSeaSyncronizer::syncAsset($asset);
            }
            usleep(250);
        } while ($cursor = Arr::get($assetResponse, 'next'));
    }
}
