<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @mixin \App\Models\Asset
 */
class AssetResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id'                  => $this->id,
            'token_id'            => $this->token_id,
            'contract_address'    => $this->contract_address,
            'image_url'           => $this->image_url,
            'image_original_url'  => $this->image_original_url,
            'image_preview_url'   => $this->image_preview_url,
            'image_thumbnail_url' => $this->image_thumbnail_url,
            'name'                => $this->name,
            'description'         => $this->description,
            'external_link'       => $this->external_link,
            'created_date'        => $this->created_date?->toDateTimeString(),
            'schema_name'         => $this->schema_name,
            'owner_address'       => $this->owner_address,
            'is_liked'            => $this->is_liked,
            'owner'               => UserResource::make($this->whenLoaded('owner')),
            'collection'          => CollectionShortResource::make($this->whenLoaded('collection')),
        ];
    }
}
