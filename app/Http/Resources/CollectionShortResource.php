<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @mixin \App\Models\Collection
 */
class CollectionShortResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id'                 => $this->id,
            'slug'               => $this->slug,
            'name'               => $this->name,
            'image_url'          => $this->image_url,
            'banner_image_url'   => $this->banner_image_url,
            'featured_image_url' => $this->featured_image_url,
            'large_image_url'    => $this->large_image_url,
        ];
    }
}
