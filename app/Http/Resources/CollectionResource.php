<?php

namespace App\Http\Resources;

use App\Models\EthPrice;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Cache;

/**
 * @mixin \App\Models\Collection
 */
class CollectionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id'                       => $this->id,
            'slug'                     => $this->slug,
            'image_url'                => $this->image_url,
            'banner_image_url'         => $this->banner_image_url,
            'featured_image_url'       => $this->featured_image_url,
            'large_image_url'          => $this->large_image_url,
            'discord_url'              => $this->discord_url,
            'telegram_url'             => $this->telegram_url,
            'wiki_url'                 => $this->wiki_url,
            'twitter_username'         => $this->twitter_username,
            'instagram_username'       => $this->instagram_username,
            'external_link'            => $this->external_link,
            'name'                     => $this->name,
            'description'              => $this->description,
            'one_day_volume'           => $this->convertPriceToUSD($this->one_day_volume),
            'one_day_change'           => $this->one_day_change,
            'one_day_sales'            => $this->one_day_sales,
            'one_day_average_price'    => $this->one_day_average_price,
            'seven_day_volume'         => $this->convertPriceToUSD($this->seven_day_volume),
            'seven_day_change'         => $this->seven_day_change,
            'seven_day_sales'          => $this->seven_day_sales,
            'seven_day_average_price'  => $this->seven_day_average_price,
            'thirty_day_volume'        => $this->convertPriceToUSD($this->thirty_day_volume),
            'thirty_day_change'        => $this->thirty_day_change,
            'thirty_day_sales'         => $this->thirty_day_sales,
            'thirty_day_average_price' => $this->thirty_day_average_price,
            'total_volume'             => $this->convertPriceToUSD($this->total_volume),
            'total_sales'              => $this->total_sales,
            'count'                    => $this->count,
            'num_owners'               => $this->num_owners,
            'average_price'            => $this->average_price,
            'num_reports'              => $this->num_reports,
            'market_cap'               => $this->market_cap,
            'total_supply'             => $this->total_supply,
            'floor_price'              => $this->floor_price,
            'created_date'             => $this->created_date?->toDateTimeString(),
        ];
    }

    private function convertPriceToUSD($price)
    {
        $eth_usd = Cache::remember('usd_price', 60, function () {
            return EthPrice::query()->latest('ethusd_updated_at')->first()?->ethusd;
        });

        return round((float)$price * $eth_usd, 2);
    }
}
