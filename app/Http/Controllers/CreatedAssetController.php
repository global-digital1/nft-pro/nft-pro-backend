<?php

namespace App\Http\Controllers;

use App\Http\Resources\AssetResource;
use Illuminate\Http\Request;

class CreatedAssetController extends Controller
{
    public function index(Request $request)
    {
        $assets = $request->user()->ownedAssets()
                          ->with([
                              'owner.media',
                              'collection',
                          ])
                          ->withIsLiked($request->user()->id)
                          ->where('contract_address', config('services.opensea.contract_address'))
                          ->orderByDesc('created_at')
                          ->orderByDesc('id')
                          ->simplePaginate();

        return AssetResource::collection($assets);
    }
}
