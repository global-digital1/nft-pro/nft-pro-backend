<?php

namespace App\Http\Controllers;

use App\Http\Resources\AssetResource;
use Illuminate\Http\Request;

class LikedAssetController extends Controller
{
    public function create(Request $request, $id)
    {
        if (!$request->user()->likedAssets()->whereKey($id)->exists()) {
            $request->user()->likedAssets()->attach($id);
        }
    }

    public function destroy(Request $request, $id)
    {
        $request->user()->likedAssets()->detach($id);
    }

    public function index(Request $request)
    {
        $assets = $request->user()
                          ->likedAssets()
                          ->with([
                              'owner.media',
                              'collection',
                          ])
                          ->withIsLiked($request->user()->id)
                          ->orderByDesc('pivot_created_at')
                          ->orderByDesc('id')
                          ->simplePaginate();

        return AssetResource::collection($assets);
    }
}
