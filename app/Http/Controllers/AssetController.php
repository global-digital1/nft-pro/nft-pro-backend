<?php

namespace App\Http\Controllers;

use App\Http\Requests\GetAssetsRequest;
use App\Http\Resources\AssetResource;
use App\Models\Asset;
use Illuminate\Http\Request;

class AssetController extends Controller
{
    public function index(GetAssetsRequest $request)
    {
        $assets = Asset::query()
                       ->with([
                           'owner.media',
                           'collection',
                       ])
                       ->withIsLiked($request->user()->id)
                       ->filter($request->filter)
                       ->order($request->order, $request->order_direction)
                       ->simplePaginate();

        return AssetResource::collection($assets);
    }

    public function show(Request $request, $id)
    {
        $asset = Asset::query()
            ->with([
                'owner.media',
                'collection',
            ])
            ->withIsLiked($request->user()->id)
            ->findOrFail($id);

        return AssetResource::make($asset);
    }
}
