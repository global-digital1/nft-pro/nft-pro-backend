<?php

namespace App\Http\Controllers;

use App\Http\Requests\MintRequest;
use App\Jobs\SyncAsset;
use App\Services\NftProMinter;

class MintController extends Controller
{
    private NftProMinter $minter;

    public function __construct(NftProMinter $minter)
    {
        $this->minter = $minter;
    }

    public function create(MintRequest $request)
    {
        $mintData = $this->minter->mint(
            $request->wallet_address ?: $request->user()->wallet_address,
            $request->file('image'),
            $request->name,
            $request->description
        );
        SyncAsset::dispatch($mintData)->delay(now()->addMinute());
    }
}
