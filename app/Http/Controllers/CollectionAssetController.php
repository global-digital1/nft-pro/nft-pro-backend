<?php

namespace App\Http\Controllers;

use App\Http\Resources\AssetResource;
use App\Models\Collection;
use Illuminate\Http\Request;

class CollectionAssetController extends Controller
{
    public function index(Request $request, $id)
    {
        $collection = Collection::query()->findOrFail($id);

        $assets = $collection->assets()
                             ->with([
                                 'owner.media',
                             ])
                             ->withIsLiked($request->user()->id)
                             ->simplePaginate();

        return AssetResource::collection($assets);
    }
}
