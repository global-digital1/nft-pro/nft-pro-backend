<?php

namespace App\Http\Controllers;

use App\Http\Requests\GetCollectionsRequest;
use App\Http\Resources\CollectionResource;
use App\Models\Collection;

class CollectionController extends Controller
{
    public function index(GetCollectionsRequest $request)
    {
        $collections = Collection::query()
                                 ->whereNotNull('image_url')
                                 ->filter($request->filter)
                                 ->order($request->order, $request->order_direction)
                                 ->simplePaginate();

        return CollectionResource::collection($collections);
    }

    public function show($id)
    {
        $collection = Collection::query()->findOrFail($id);

        return CollectionResource::make($collection);
    }
}
