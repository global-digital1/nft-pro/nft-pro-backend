<?php

namespace App\Http\Controllers;

use App\Http\Resources\AssetResource;
use App\Models\NftOfTheDay;
use Illuminate\Http\Request;

class NftOfTheDayController extends Controller
{
    public function index(Request $request)
    {
        $asset = NftOfTheDay::query()->where('date', now()->toDateString())
                            ->firstOrFail()
                            ->asset()
                            ->with([
                                'owner.media',
                                'collection',
                            ])
                            ->withIsLiked($request->user()->id)
                            ->firstOrFail();

        return AssetResource::make($asset);
    }
}
