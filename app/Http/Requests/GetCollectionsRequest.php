<?php

namespace App\Http\Requests;

use App\Enums\CollectionOrder;
use App\Enums\OrderDirection;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rules\In;

class GetCollectionsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'order'           => ['sometimes', 'string', new In(CollectionOrder::toArray())],
            'order_direction' => ['sometimes', 'string', new In(OrderDirection::toArray())],
            'filter'          => ['sometimes', 'string', 'max:255'],
        ];
    }
}
