<?php

namespace App\Http\Requests;

use App\Enums\AssetOrder;
use App\Enums\OrderDirection;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rules\In;

class GetAssetsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'order'           => ['sometimes', 'string', new In(AssetOrder::toArray())],
            'order_direction' => ['sometimes', 'string', new In(OrderDirection::toArray())],
            'filter'          => ['sometimes', 'string', 'max:255'],
        ];
    }
}
