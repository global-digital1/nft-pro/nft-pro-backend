<?php

namespace App\Services;

use Illuminate\Support\Facades\Http;

class EtherScan
{
    private string $host;
    private string $key;

    public function __construct(string $host, string $key)
    {
        $this->host = $host;
        $this->key = $key;
    }

    public function getLastPrice()
    {
        return Http::get($this->host, [
            'module' => 'stats',
            'action' => 'ethprice',
            'apikey' => $this->key,
        ])->json()['result'];
    }
}
