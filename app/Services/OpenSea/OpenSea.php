<?php

namespace App\Services\OpenSea;

use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;

class OpenSea
{

    private string $host;
    private string $key;

    public function __construct(string $host, string $key)
    {
        $this->host = $host;
        $this->key  = $key;
    }

    public function getAssets($params = [])
    {
        return Http::retry(240, 5000)->get($this->host . '/assets', $params)
                   ->json();

    }

    public function getCollections($params = [])
    {
        return Http::retry(240, 5000)->get($this->host . '/collections', $params)
                   ->json();
    }

    public function getCollection($slug)
    {
        return Http::retry(240, 5000)->get($this->host . '/collection/' . $slug)
                   ->json();
    }

    public function getAsset($contractAddress, $tokenId)
    {
        return Http::retry(240, 5000)->get($this->host . '/asset/' . $contractAddress . '/' . $tokenId)
                   ->json();
    }
}
