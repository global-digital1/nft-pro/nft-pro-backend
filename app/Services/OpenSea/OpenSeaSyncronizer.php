<?php

namespace App\Services\OpenSea;

use App\Models\Asset;
use App\Models\Collection;
use Illuminate\Support\Carbon;
use Illuminate\Support\Str;

class OpenSeaSyncronizer
{
    public static function syncCollection($collectionData)
    {
        return Collection::query()->updateOrCreate([
            'slug' => $collectionData['slug'],
        ], [
            'image_url'                => $collectionData['image_url'],
            'banner_image_url'         => $collectionData['banner_image_url'],
            'featured_image_url'       => $collectionData['featured_image_url'],
            'large_image_url'          => $collectionData['large_image_url'],
            'discord_url'              => $collectionData['discord_url'],
            'telegram_url'             => $collectionData['telegram_url'],
            'wiki_url'                 => $collectionData['wiki_url'],
            'twitter_username'         => $collectionData['twitter_username'],
            'instagram_username'       => $collectionData['instagram_username'],
            'name'                     => $collectionData['name'],
            'external_link'            => $collectionData['external_url'],
            'description'              => Str::substr($collectionData['description'], 0, 10000),
            'one_day_volume'           => $collectionData['stats']['one_day_volume'],
            'one_day_change'           => $collectionData['stats']['one_day_change'],
            'one_day_sales'            => $collectionData['stats']['one_day_sales'],
            'one_day_average_price'    => $collectionData['stats']['one_day_average_price'],
            'seven_day_volume'         => $collectionData['stats']['seven_day_volume'],
            'seven_day_change'         => $collectionData['stats']['seven_day_change'],
            'seven_day_sales'          => $collectionData['stats']['seven_day_sales'],
            'seven_day_average_price'  => $collectionData['stats']['seven_day_average_price'],
            'thirty_day_volume'        => $collectionData['stats']['thirty_day_volume'],
            'thirty_day_change'        => $collectionData['stats']['thirty_day_change'],
            'thirty_day_sales'         => $collectionData['stats']['thirty_day_sales'],
            'thirty_day_average_price' => $collectionData['stats']['thirty_day_average_price'],
            'total_volume'             => $collectionData['stats']['total_volume'],
            'total_sales'              => $collectionData['stats']['total_sales'],
            'count'                    => $collectionData['stats']['count'],
            'num_owners'               => $collectionData['stats']['num_owners'],
            'average_price'            => $collectionData['stats']['average_price'],
            'num_reports'              => $collectionData['stats']['num_reports'],
            'market_cap'               => $collectionData['stats']['market_cap'],
            'total_supply'             => $collectionData['stats']['total_supply'],
            'floor_price'              => $collectionData['stats']['floor_price'],
            'created_date'             => Carbon::make($collectionData['created_date']),
        ]);
    }

    public static function syncAsset($assetData)
    {
        return Asset::query()->updateOrCreate([
            'token_id'         => $assetData['token_id'],
            'contract_address' => $assetData['asset_contract']['address'],
        ], [
            'collection_slug'     => $assetData['collection']['slug'],
            'schema_name'         => $assetData['asset_contract']['schema_name'],
            'created_date'        => Carbon::make($assetData['asset_contract']['created_date']),
            'owner_address'       => self::getOwnerAddress($assetData),
            'num_sales'           => $assetData['num_sales'],
            'image_url'           => $assetData['image_url'],
            'image_preview_url'   => $assetData['image_preview_url'],
            'image_thumbnail_url' => $assetData['image_thumbnail_url'],
            'image_original_url'  => $assetData['image_original_url'],
            'name'                => $assetData['name'],
            'external_link'       => $assetData['external_link'],
            'description'         => Str::substr($assetData['description'], 0, 10000),
        ]);
    }

    private static function getOwnerAddress($data)
    {
        if(
            $data['owner']['address'] === '0x0000000000000000000000000000000000000000'
            && isset($data['top_ownerships'])
            && $data['top_ownerships'] !== null
        ){
            return $data['top_ownerships'][0]['owner']['address'];
        }
        return  $data['owner']['address'];
    }
}
