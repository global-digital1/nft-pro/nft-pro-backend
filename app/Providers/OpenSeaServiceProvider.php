<?php

namespace App\Providers;

use App\Services\OpenSea\OpenSea;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\Support\DeferrableProvider;
use Illuminate\Support\ServiceProvider;

class OpenSeaServiceProvider extends ServiceProvider implements DeferrableProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(OpenSea::class, function (Application $app) {
            return new OpenSea(config('services.opensea.host'), config('services.opensea.key'));
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    public function provides()
    {
        return [
            OpenSea::class
        ];
    }
}
