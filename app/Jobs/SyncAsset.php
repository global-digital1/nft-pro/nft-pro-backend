<?php

namespace App\Jobs;

use App\Services\OpenSea\OpenSea;
use App\Services\OpenSea\OpenSeaSyncronizer;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class SyncAsset implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $mintData;

    public function __construct($mintData)
    {
        $this->mintData = $mintData;
    }

    public function handle(OpenSea $openSea)
    {
        $assetData = $openSea->getAsset($this->mintData['contractAddress'], $this->mintData['tokenId']);
        OpenSeaSyncronizer::syncAsset($assetData);
    }
}
