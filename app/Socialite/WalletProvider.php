<?php

namespace App\Socialite;

use Illuminate\Support\Arr;
use Laravel\Socialite\Two\AbstractProvider;
use Laravel\Socialite\Two\ProviderInterface;
use Laravel\Socialite\Two\User;

class WalletProvider extends AbstractProvider implements ProviderInterface
{
    protected function getTokenUrl()
    {
        return null;
    }

    protected function mapUserToObject(array $user)
    {
        return (new User)->setRaw($user)->map([
            'id'       => Arr::get($user, 'id'),
            'nickname' => Arr::get($user, 'firstName'),
            'name'     => Arr::get($user, 'firstName') . ' ' . Arr::get($user, 'lastName'),
            'email'    => Arr::get($user, 'email'),
        ]);
    }

    protected function getUserByToken($token)
    {
        $host = config('services.rnetwork.host') ?: 'https://rnetwork.io';
        $response = $this->getHttpClient()->get("$host/api/appuser", [
            'query' => [
                'token'       => $token,
                'resubscribe' => 0,
            ],
        ]);

        return json_decode($response->getBody(), true, 512, JSON_THROW_ON_ERROR)['result'];
    }

    protected function getAuthUrl($state)
    {
        return null;
    }
}
