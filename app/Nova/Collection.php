<?php

namespace App\Nova;

use App\Models\Collection as CollectionModel;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\Date;
use Laravel\Nova\Fields\HasMany;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Number;
use Laravel\Nova\Fields\Text;

class Collection extends Resource
{
    public static $model = CollectionModel::class;

    public static $title = 'name';

    public static $search = [
        'id', 'slug', 'name'
    ];

    public function fields(Request $request): array
    {
        return [
            ID::make()->sortable(),

            Text::make('Slug')
                ->sortable()
                ->rules('required'),

            Text::make('Image Url')
                ->sortable()
                ->rules('nullable'),

            Text::make('Name')
                ->sortable()
                ->rules('nullable'),

            Text::make('External Link')
                ->sortable()
                ->rules('nullable'),

            Text::make('Description')
                ->sortable()
                ->rules('nullable'),

            Number::make('Total Supply')
                  ->sortable()
                  ->rules('nullable', 'integer'),

            Text::make('Floor Price')
                ->sortable()
                ->rules('nullable'),

            Date::make('Created Date')
                ->sortable()
                ->rules('nullable', 'date'),

            Text::make('One Day Volume')
                ->sortable()
                ->rules('nullable'),

            Text::make('One Day Change')
                ->sortable()
                ->rules('nullable'),

            Text::make('One Day Sales')
                ->sortable()
                ->rules('nullable'),

            Text::make('One Day Average Price')
                ->sortable()
                ->rules('nullable'),

            Text::make('Seven Day Volume')
                ->sortable()
                ->rules('nullable'),

            Text::make('Seven Day Change')
                ->sortable()
                ->rules('nullable'),

            Text::make('Seven Day Sales')
                ->sortable()
                ->rules('nullable'),

            Text::make('Seven Day Average Price')
                ->sortable()
                ->rules('nullable'),

            Text::make('Thirty Day Volume')
                ->sortable()
                ->rules('nullable'),

            Text::make('Thirty Day Change')
                ->sortable()
                ->rules('nullable'),

            Text::make('Thirty Day Sales')
                ->sortable()
                ->rules('nullable'),

            Text::make('Thirty Day Average Price')
                ->sortable()
                ->rules('nullable'),

            Text::make('Total Volume')
                ->sortable()
                ->rules('nullable'),

            Text::make('Total Sales')
                ->sortable()
                ->rules('nullable'),

            Text::make('Count')
                ->sortable()
                ->rules('nullable'),

            Text::make('Num Owners')
                ->sortable()
                ->rules('nullable'),

            Text::make('Average Price')
                ->sortable()
                ->rules('nullable'),

            Text::make('Num Reports')
                ->sortable()
                ->rules('nullable'),

            Text::make('Market Cap')
                ->sortable()
                ->rules('nullable'),

            Text::make('Banner Image Url')
                ->sortable()
                ->rules('nullable'),

            Text::make('Featured Image Url')
                ->sortable()
                ->rules('nullable'),

            Text::make('Large Image Url')
                ->sortable()
                ->rules('nullable'),

            Text::make('Discord Url')
                ->sortable()
                ->rules('nullable'),

            Text::make('Telegram Url')
                ->sortable()
                ->rules('nullable'),

            Text::make('Wiki Url')
                ->sortable()
                ->rules('nullable'),

            Text::make('Twitter Username')
                ->sortable()
                ->rules('nullable'),

            Text::make('Instagram Username')
                ->sortable()
                ->rules('nullable'),

            HasMany::make('Assets', 'assets', Asset::class),
        ];
    }

    public function cards(Request $request): array
    {
        return [];
    }

    public function filters(Request $request): array
    {
        return [];
    }

    public function lenses(Request $request): array
    {
        return [];
    }

    public function actions(Request $request): array
    {
        return [];
    }
}
