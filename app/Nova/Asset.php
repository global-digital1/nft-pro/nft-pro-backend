<?php

namespace App\Nova;

use App\Models\Asset as AssetModel;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Number;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Http\Requests\NovaRequest;

class Asset extends Resource
{
    public static $model = AssetModel::class;

    public static $title = 'name';

    public static $search = [
        'id', 'contract_address', 'owner_address', 'name',
    ];

    public static $relatableSearchResults = 50;

    public function fields(Request $request): array
    {
        return [
            ID::make()->sortable(),

            Text::make('Token Id')
                ->sortable()
                ->rules('required'),

            Text::make('Contract Address')
                ->sortable()
                ->rules('required'),

            Text::make('Owner Address')
                ->sortable()
                ->rules('nullable'),

            Text::make('Image Url')
                ->sortable()
                ->rules('nullable'),

            Text::make('Image Preview Url')
                ->sortable()
                ->rules('nullable'),

            Text::make('Image Thumbnail Url')
                ->sortable()
                ->rules('nullable'),

            Text::make('Image Original Url')
                ->sortable()
                ->rules('nullable'),

            Text::make('Name')
                ->sortable()
                ->rules('nullable'),

            Text::make('External Link')
                ->sortable()
                ->rules('nullable'),

            Text::make('Description')
                ->sortable()
                ->rules('nullable'),

            Number::make('Num Sales')
                  ->sortable()
                  ->rules('nullable', 'integer'),

        ];
    }

    public function cards(Request $request): array
    {
        return [];
    }

    public function filters(Request $request): array
    {
        return [];
    }

    public function lenses(Request $request): array
    {
        return [];
    }

    public function actions(Request $request): array
    {
        return [];
    }
}
