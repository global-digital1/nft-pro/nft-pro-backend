<?php

namespace App\Nova;

use App\Models\NftOfTheDay as NftOfTheDayModel;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\Date;
use Laravel\Nova\Fields\ID;

class NftOfTheDay extends Resource
{
    public static $model = NftOfTheDayModel::class;

    public static $title = 'id';

    public function fields(Request $request): array
    {
        return [
            ID::make()->sortable(),

            Date::make('Date'),

            BelongsTo::make('Asset')
                     ->rules('required')
                     ->searchable(),
        ];
    }

    public function cards(Request $request): array
    {
        return [];
    }

    public function filters(Request $request): array
    {
        return [];
    }

    public function lenses(Request $request): array
    {
        return [];
    }

    public function actions(Request $request): array
    {
        return [];
    }

    public static function label()
    {
        return 'NFTs Of The Day';
    }
}
