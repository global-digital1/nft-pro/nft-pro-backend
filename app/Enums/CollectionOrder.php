<?php

namespace App\Enums;

use Spatie\Enum\Laravel\Enum;

/**
 *@method static self created_date()
 *@method static self floor_price()
 *@method static self total_supply()
 *@method static self total_sales()
 *@method static self one_day_sales()
 *@method static self seven_day_sales()
 *@method static self thirty_day_sales()
 */
class CollectionOrder extends Enum
{

}
