<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCollectionsTable extends Migration
{
    public function up()
    {
        Schema::create('collections', function (Blueprint $table) {
            $table->id();
            $table->string('slug');
            $table->string('image_url')->nullable();
            $table->string('name')->nullable();
            $table->string('external_link')->nullable();
            $table->text('description')->nullable();
            $table->integer('total_supply')->nullable();
            $table->string('floor_price')->nullable();
            $table->timestamp('created_date')->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('collections');
    }
}
