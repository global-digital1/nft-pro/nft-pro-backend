<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEthPricesTable extends Migration
{
    public function up()
    {
        Schema::create('eth_prices', function (Blueprint $table) {
            $table->string('ethbtc');
            $table->timestamp('ethbtc_updated_at');
            $table->string('ethusd');
            $table->timestamp('ethusd_updated_at');
        });
    }

    public function down()
    {
        Schema::dropIfExists('eth_prices');
    }
}
