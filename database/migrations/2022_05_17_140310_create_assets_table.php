<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAssetsTable extends Migration
{
    public function up()
    {
        Schema::create('assets', function (Blueprint $table) {
            $table->id();
            $table->string('token_id')->nullable();
            $table->string('contract_address');
            $table->string('collection_slug');
            $table->string('image_url')->nullable();
            $table->string('image_preview_url')->nullable();
            $table->string('image_thumbnail_url')->nullable();
            $table->string('image_original_url')->nullable();
            $table->string('name')->nullable();
            $table->string('external_link')->nullable();
            $table->text('description')->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('assets');
    }
}
