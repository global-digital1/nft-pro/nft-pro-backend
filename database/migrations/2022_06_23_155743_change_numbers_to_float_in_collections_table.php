<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('collections', function (Blueprint $table) {
            $table->float('one_day_volume', 36, 18)->nullable()->change();
            $table->float('one_day_change', 36, 18)->nullable()->change();
            $table->integer('one_day_sales')->nullable()->change();
            $table->float('one_day_average_price', 36, 18)->nullable()->change();
            $table->float('seven_day_volume', 36, 18)->nullable()->change();
            $table->float('seven_day_change', 36, 18)->nullable()->change();
            $table->integer('seven_day_sales')->nullable()->change();
            $table->float('seven_day_average_price', 36, 18)->nullable()->change();
            $table->float('thirty_day_volume', 36, 18)->nullable()->change();
            $table->float('thirty_day_change', 36, 18)->nullable()->change();
            $table->integer('thirty_day_sales')->nullable()->change();
            $table->float('thirty_day_average_price', 36, 18)->nullable()->change();
            $table->float('total_volume', 36, 18)->nullable()->change();
            $table->float('average_price', 36, 18)->nullable()->change();
            $table->integer('total_sales')->nullable()->change();
            $table->integer('count')->nullable()->change();
            $table->integer('num_owners')->nullable()->change();
            $table->integer('num_reports')->nullable()->change();
            $table->float('market_cap', 36, 18)->nullable()->change();
            $table->float('floor_price', 36, 18)->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('collections', function (Blueprint $table) {
            $table->string('one_day_volume')->nullable()->change();
            $table->string('one_day_change')->nullable()->change();
            $table->string('one_day_sales')->nullable()->change();
            $table->string('one_day_average_price')->nullable()->change();
            $table->string('seven_day_volume')->nullable()->change();
            $table->string('seven_day_change')->nullable()->change();
            $table->string('seven_day_sales')->nullable()->change();
            $table->string('seven_day_average_price')->nullable()->change();
            $table->string('thirty_day_volume')->nullable()->change();
            $table->string('thirty_day_change')->nullable()->change();
            $table->string('thirty_day_sales')->nullable()->change();
            $table->string('thirty_day_average_price')->nullable()->change();
            $table->string('total_volume')->nullable()->change();
            $table->string('average_price')->nullable()->change();
            $table->string('total_sales')->nullable()->change();
            $table->string('count')->nullable()->change();
            $table->string('num_owners')->nullable()->change();
            $table->string('num_reports')->nullable()->change();
            $table->string('market_cap')->nullable()->change();
            $table->string('floor_price')->nullable()->change();
        });
    }
};
