<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('collections', function (Blueprint $table) {
            $table->text('image_url')->nullable()->change();
            $table->text('banner_image_url')->nullable()->change();
            $table->text('featured_image_url')->nullable()->change();
            $table->text('large_image_url')->nullable()->change();
            $table->text('external_link')->nullable()->change();
            $table->text('discord_url')->nullable()->change();
            $table->text('telegram_url')->nullable()->change();
            $table->text('wiki_url')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('collections', function (Blueprint $table) {
            $table->string('image_url')->nullable()->change();
            $table->string('banner_image_url')->nullable()->change();
            $table->string('featured_image_url')->nullable()->change();
            $table->string('large_image_url')->nullable()->change();
            $table->string('external_link')->nullable()->change();
            $table->string('discord_url')->nullable()->change();
            $table->string('telegram_url')->nullable()->change();
            $table->string('wiki_url')->nullable()->change();
        });
    }
};
