<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('assets', function (Blueprint $table) {
            $table->text('image_url')->nullable()->change();
            $table->text('image_preview_url')->nullable()->change();
            $table->text('image_thumbnail_url')->nullable()->change();
            $table->text('image_original_url')->nullable()->change();
            $table->text('external_link')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('assets', function (Blueprint $table) {
            $table->string('image_url')->nullable()->change();
            $table->string('image_preview_url')->nullable()->change();
            $table->string('image_thumbnail_url')->nullable()->change();
            $table->string('image_original_url')->nullable()->change();
            $table->string('external_link')->nullable()->change();
        });
    }
};
