<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('collections', function (Blueprint $table) {
            $table->string('one_day_volume')->nullable();
            $table->string('one_day_change')->nullable();
            $table->string('one_day_sales')->nullable();
            $table->string('one_day_average_price')->nullable();
            $table->string('seven_day_volume')->nullable();
            $table->string('seven_day_change')->nullable();
            $table->string('seven_day_sales')->nullable();
            $table->string('seven_day_average_price')->nullable();
            $table->string('thirty_day_volume')->nullable();
            $table->string('thirty_day_change')->nullable();
            $table->string('thirty_day_sales')->nullable();
            $table->string('thirty_day_average_price')->nullable();
            $table->string('total_volume')->nullable();
            $table->string('total_sales')->nullable();
            $table->string('count')->nullable();
            $table->string('num_owners')->nullable();
            $table->string('average_price')->nullable();
            $table->string('num_reports')->nullable();
            $table->string('market_cap')->nullable();

            $table->string('banner_image_url')->nullable();
            $table->string('featured_image_url')->nullable();
            $table->string('large_image_url')->nullable();
            $table->string('discord_url')->nullable();
            $table->string('telegram_url')->nullable();
            $table->string('wiki_url')->nullable();
            $table->string('twitter_username')->nullable();
            $table->string('instagram_username')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('collections', function (Blueprint $table) {
            $table->dropColumn('one_day_volume');
            $table->dropColumn('one_day_change');
            $table->dropColumn('one_day_sales');
            $table->dropColumn('one_day_average_price');
            $table->dropColumn('seven_day_volume');
            $table->dropColumn('seven_day_change');
            $table->dropColumn('seven_day_sales');
            $table->dropColumn('seven_day_average_price');
            $table->dropColumn('thirty_day_volume');
            $table->dropColumn('thirty_day_change');
            $table->dropColumn('thirty_day_sales');
            $table->dropColumn('thirty_day_average_price');
            $table->dropColumn('total_volume');
            $table->dropColumn('total_sales');
            $table->dropColumn('count');
            $table->dropColumn('num_owners');
            $table->dropColumn('average_price');
            $table->dropColumn('num_reports');
            $table->dropColumn('market_cap');

            $table->dropColumn('banner_image_url');
            $table->dropColumn('featured_image_url');
            $table->dropColumn('large_image_url');
            $table->dropColumn('discord_url');
            $table->dropColumn('telegram_url');
            $table->dropColumn('wiki_url');
            $table->dropColumn('twitter_username');
            $table->dropColumn('instagram_username');
        });
    }
};
